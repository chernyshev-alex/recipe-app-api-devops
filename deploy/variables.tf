variable "prefix" {
  default = "raad"
}

variable "project" {
    default = "recipe-app-api-devops"
}

variable "contact" {
    default = "chernyshev.alexander@gmail.com"
}

variable "db_username" {
  description = "username for RDS postgres instance"
}

variable "db_password" {
  description = "password for RDS postgres instance"
}
variable "bastion_key_name" {
  default = "recipe-api-app-devops-bastion"
}
variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "135999069915.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "135999069915.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}
variable "dns_zone_name" {
  description = "Domain name"
  default     = "my-registered-domain-name.net"
}
variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "api"
    staging    = "api.staging"
    dev        = "api.dev"
  }
}

